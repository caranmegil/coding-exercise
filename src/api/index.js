import facilitiesData from './facilities.json';
import reviewersData from './reviewers.json';
import reviewsData from './reviews.json';

let facilities = facilitiesData;
let reviewers = reviewersData;
let reviews = reviewsData;

export const getFacilities = async () => {
  console.log('Fetching facilities...');

  await new Promise(resolve => {
    setTimeout(resolve, 1500);
  });

  console.log('Fetched facilities.')
  console.log(facilities);

  return facilities;
};

export const getReviewers = async () => {
  console.log('Fetching reviewers...');

  await new Promise(resolve => {
    setTimeout(resolve, 1500);
  });

  console.log('Fetched reviewers.')
  console.log(reviewers);

  return reviewers;
};

export const createReviewer = async (reviewer) => {
  console.log('Creating reviewer...');

  await new Promise(resolve => {
    setTimeout(resolve, 1500);
  });

  reviewers = [
    ...reviewers,
    { id: reviewers[reviewers.length - 1].id + 1, ...reviewer }
  ];

  console.log('Created reviewer.')
  console.log(reviewer);

  return reviewers[reviewers.length - 1];
};

export const getReviews = async () => {
  console.log('Fetching reviews...');

  await new Promise(resolve => {
    setTimeout(resolve, 1500);
  });

  console.log('Fetched reviews.')
  console.log(reviews);

  return reviews;
};

export const createReview = async (review) => {
  console.log('Creating review...');

  await new Promise(resolve => {
    setTimeout(resolve, 1500);
  });

  if (!review.rating) {
    throw new Error('Review must have a rating.');
  }

  if (![1, 2, 3, 4, 5].includes(review.rating)) {
    throw new Error('Review must be between 1 and 5.');
  }

  if (!review.facilityId) {
    throw new Error('Review must be associated with a facility.');
  }

  reviews = [
    ...reviews,
    { id: reviews[reviews.length - 1].id + 1, ...review }
  ];


  console.log('Created review.')
  console.log(review);

  return reviews[reviews.length - 1];
}
