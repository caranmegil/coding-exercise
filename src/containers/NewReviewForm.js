import React, { useEffect, useState } from 'react';
import { getFacilities } from '../api';
import ReviewForm from '../components/ReviewForm';

const NewReviewForm = () => {
  const [facilities, setFacilities] = useState([]);
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    getFacilities()
      .then(res => setFacilities(res))
      .finally(() => setLoading(false));
  }, []);

  return (
    <ReviewForm facilities={facilities} loading={loading} />
  )
};

export default NewReviewForm;
