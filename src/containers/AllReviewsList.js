import React, { useEffect, useContext, useState } from 'react';
import { getReviews, getReviewers } from '../api';
import ReviewsList from '../components/ReviewsList';
import reviewsContext from '../components/Reviews/reviewsContext';

const AllReviewsList = () => {
  const [reviews, setReviews] = useState([]);
  const [loading, setLoading] = useState(true);
  const [reviewsDetails, setReviewsDetails] = useContext(reviewsContext);

  useEffect(() => {
    if (reviewsDetails.reloadReviews) {
      getReviewers()
        .then(reviewersData => {
          getReviews()
            .then(reviewsData => {
              const formattedReviews = reviewsData.map(review => {
                if (!review.reviewerId) {
                  return review;
                }

                return {
                  ...review,
                  reviewer: reviewersData.find(reviewer => reviewer.id === review.reviewerId),
                };
              });

              setReviews(formattedReviews);
            })
            .finally(() => {
              setReviewsDetails({
                ...reviewsDetails,
                reloadReviews: false,
              })
              setLoading(false)
            });
      });
    }
  }, [reviewsDetails.reloadReviews]);

  return (
    <ReviewsList reviews={reviews} loading={loading} />
  );
};

export default AllReviewsList;
