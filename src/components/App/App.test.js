import { render, screen } from '@testing-library/react';
import App from '../../App';

test('renders component to finish', () => {
  render(<App />);
  const linkElement = screen.getByText(/Finish me/i);
  expect(linkElement).toBeInTheDocument();
});
