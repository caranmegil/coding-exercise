import React from 'react';
import Reviews from '../Reviews';
import logo from './logo.svg';
import './styles.css';

 /*
    Hi! Start your exercise in the Reviews.js component.
  */
const App = () => (
  <>
    <div className="content">
      <Reviews />
    </div>
    <div className="footer">
      <div className="container">
        <img src={logo} alt="Storable Websites" className="logo" />
      </div>
    </div>
  </>
);

export default App;
