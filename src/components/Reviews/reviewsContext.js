import React, { useState } from 'react';

export const reviewsInfo = {
    reloadReviews: true,
}
  
const reviewsContext = React.createContext(reviewsInfo);

export const Provider = (props) => {
  const [state, setState] = useState(reviewsInfo);
  return (
    <reviewsContext.Provider value={[state, setState]}>
      {props.children}
    </reviewsContext.Provider>
  );
}

export default reviewsContext;