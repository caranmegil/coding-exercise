import React from 'react';
import AllReviewsList from '../../containers/AllReviewsList';
import NewReviewForm from '../../containers/NewReviewForm';
import './styles.css';
import { Provider } from './reviewsContext';

const Reviews = () => {
  return (
    <Provider>
      <div className="container">
        <NewReviewForm />
        <AllReviewsList />
      </div>
    </Provider>
  );
};

export default Reviews;
