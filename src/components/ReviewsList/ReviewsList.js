import React from 'react';
import './styles.css';

const ReviewsList = ({ reviews, loading }) => {
  return (
    <div className="reviews-list">
      <h5>Reviews</h5>
      <div className="reviews-card-list">
      {
        !loading && reviews.map((review) => {
          const dateObj = new Date(Date.parse(review.createdAt));
          let dateStr = dateObj.getMonth() + '/' + dateObj.getDate() + '/' + dateObj.getFullYear();
          return (
            <div className="card" style={{width: '18rem', height: '15rem'}} key={review.id}>
              <div className="card-body">
                <div className="card-title"><h5 className="item">{review.rating} / 5</h5><h6 className="card-subtitle mb-2 text-muted item">{review.reviewer.name}</h6></div>
                <div className="card-text">{review.body}</div>
                <div className="card-subtitle mb-2 text-muted date">{dateStr}</div>
              </div>
            </div>
          )
        })
      }
      </div>
    </div>
  );
};

export default ReviewsList;
