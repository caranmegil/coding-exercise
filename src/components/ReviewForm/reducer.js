export const CHANGE_RATING = 'rating/change';
export const CHANGE_FACILITY_ID = 'facilityId/change';
export const CHANGE_BODY = 'body/change';
export const CHANGE_NAME = 'name/change';
export const CHANGE_EMAIL = 'email/change';

export const initialState = {
  rating: '',
  facilityId: '0',
  body: '',
  name: '',
  email: '',
};

const reducer = (state, action) => {
  switch (action.type) {
    case CHANGE_RATING:
      return {
        ...state,
        rating: action.rating,
      };
    case CHANGE_FACILITY_ID:
      return {
        ...state,
        facilityId: action.facilityId,
      };
    case CHANGE_BODY:
      return {
        ...state,
        body: action.body,
      };
    case CHANGE_NAME:
      return {
        ...state,
        name: action.name,
      };
    case CHANGE_EMAIL:
      return {
        ...state,
        email: action.email,
      };
    default:
      return state;
  }
};

export default reducer;
