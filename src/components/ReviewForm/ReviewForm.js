import React, { useReducer, useState, useContext } from 'react';
import reducer, { initialState, CHANGE_RATING, CHANGE_FACILITY_ID, CHANGE_BODY, CHANGE_NAME, CHANGE_EMAIL } from './reducer';
import { createReview } from '../../api';
import reviewsContext from '../Reviews/reviewsContext';

const ReviewForm = ({ facilities, loading }) => {
  const [state, dispatch] = useReducer(reducer, initialState);
  const [reviewsState, setReviewsState] = useContext(reviewsContext);
  const [formState, setFormState] = useState({error: null})

  const onSubmit = e => {
    createReview({
      ...state,
      createdAt: new Date().toISOString(),
      rating: parseInt(state.rating),
      reviewer: {
        name: state.name,
        email: state.email,
      },
    }).then( () => {
      setReviewsState({...reviewsState, reloadReviews: true})
      setFormState({
        ...formState,
        error: null,
      })
    })
    .catch(e => setFormState({...formState, error: e}));  
    e.preventDefault();
  };

  return (
    <div className="card">
      <h5 className="card-header">Write a Review</h5>
      <div className="card-body">
        { (formState.error != null) ? (
            <div class="alert alert-danger" role="alert">{formState.error}</div>
          )
          : null
        }
        <form onSubmit={onSubmit}>
          <div className="form-group">
            {[1, 2, 3, 4, 5].map(rating => (
              <div key={rating} className="form-check form-check-inline">
                <input
                  className="form-check-input"
                  type="radio"
                  name="rating"
                  id={`rating${rating}`}
                  value={rating}
                  required
                  onChange={e => dispatch({ type: CHANGE_RATING, rating: e.target.value })}
                />
                <label className="form-check-label" htmlFor={`rating${rating}`}>{rating}</label>
              </div>
            ))}
            <strong className="required">*</strong>
          </div>
          <div className="form-group">
            <label htmlFor="facilityId">Facility <strong className="required">*</strong></label>
            <select
              className="form-control"
              id="facilityId"
              onChange={e => dispatch({ type: CHANGE_FACILITY_ID, facilityId: e.target.value })}
              disabled={loading}
            >
              {facilities.map(facility => (
                <option key={facility.id} value={facility.id}>{facility.name}</option>
              ))}
            </select>
          </div>
          <div className="form-group">
            <label htmlFor="body">Review</label>
            <textarea
              className="form-control"
              id="body"
              rows="3"
              onChange={e => dispatch({ type: CHANGE_BODY, body: e.target.value })}
            />
          </div>
          <div className="form-group">
            <label htmlFor="name">Name</label>
            <input
              type="text"
              className="form-control"
              id="name"
              onChange={e => dispatch({ type: CHANGE_NAME, name: e.target.value })}
            />
          </div>
          <div className="form-group">
            <label htmlFor="email">Email</label>
            <input
              type="email"
              className="form-control"
              id="email"
              onChange={e => dispatch({ type: CHANGE_EMAIL, email: e.target.value })}
            />
          </div>
          <button type="submit" className="btn btn-primary">Submit</button>
        </form>
      </div>
    </div>
  )
};

export default ReviewForm;
