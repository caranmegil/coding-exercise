# Frontend Coding Exercise

## Task ⌨️

For this exercise, you will be implementing the ability for a website visitor to submit a review for a self storage facility.

### Requirements

- the form for creating a new review must submit successfully
  - a rating from 1 to 5 is **required**
  - the facility being reviewed is **required**
  - the review body is optional
  - the reviewer's name is optional
  - the reviewer's email is optional
- reviews for all facilities are displayed in rows of three below the new review form
- on successful form submit, the rows of reviews will be updated to include the newest review

### Styling

How and what you use to style your submission is up to you! [Bootstrap 4.6.0](https://getbootstrap.com/docs/4.6/getting-started/introduction/) is imported in `index.js` for your convenience. You can use the following image as inspiration. Don't worry, the form's UI is already completed for you!

![Example](./example.png)

## Dependencies 🔧

- [Node](https://nodejs.org/en/) (at least `12.*`)
- [Yarn](https://classic.yarnpkg.com/lang/en/)

## Getting Started 🏃‍♀️

First, install the dependencies:

```bash
yarn install
```

Then, start the development server:

```bash
yarn start
```

The [`ReviewForm`](./src/components/ReviewForm/ReviewForm.js) and [`ReviewsList`](./src/components/ReviewsList/ReviewsList.js) components are your starting points. You do not have to worry about submitting the form to a real endpoint; an API module is provided that mocks network calls to create and retrieve the necessary data. You can look at the code inside the [API module](./src/api/index.js), but do not need to touch it as part of the exercise.

As you're working through this, be sure to apply the same standards as you would on a real app. Additionally, feel free to use the `NOTES.md` file to document any resources you used, assumptions or tradeoffs you made in your implementation, and anything else you'd like the reviewers to read.

Have fun! 💖
